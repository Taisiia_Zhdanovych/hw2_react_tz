
import { Component } from "react";

import ProductList from "../components/ProductList/ProductList";
import Header from "../components/Header/Header";

 import Modal from "../components/Modal/Modal";

import "./home.scss"




export default class Home extends Component {
    constructor(props) {
      super(props);
      
      this.state = {
            cardProducts: [],
            favorits: [],
            cart: [],
          
            modalActive: false,
            modalId: 0,
        }
        }
    
 componentDidMount() {
        fetch('./ProductData.json')
            .then(response => response.json())
     .then(products => this.setState({ cardProducts: products }));
   
   if (localStorage.getItem("favorits")!=null) {
     this.setState({
       favorits: JSON.parse(localStorage.getItem("favorits"))
     })
   }
   if (localStorage.getItem("cart") != null) {
     this.setState({
       cart: JSON.parse(localStorage.getItem("cart"))
     })
   }

    }

  componentDidUpdate(prevState) {
    if (this.state.favorits !== prevState.favorits) {
     localStorage.setItem("favorits", JSON.stringify(this.state.favorits))
    }

     if (this.state.cart !== prevState.cart) {
     localStorage.setItem("cart", JSON.stringify(this.state.cart))
    }
  }
  
  addToFavorite=(id)=> {
     
        
        this.setState((prev) => {
            if (prev.favorits.includes(id)) {
                const newArrD = prev.favorits.filter((item) => { return item !== id });
              
                return { favorits: newArrD }
            }
            else {
              
                return { favorits: [...prev.favorits, id]  }
            }
        })
      
  }
  
  
 
  
  setModalActive = (active, id) => {
  const modalUpdates = {
    modalId: id,
    modalActive: active,
    headerText: "",
    mainText: ""
  };

  this.setState((prev) => {
    if (prev.cart.includes(id)) {
      modalUpdates.headerText = "Зверніть увагу";
      modalUpdates.mainText = "Toвар вже у корзині";
    } else {
      modalUpdates.headerText = "Додати товар у корзину";
      modalUpdates.mainText = "Чи дійсно ви бажаєте додати товар у корзину?";
    }

    return modalUpdates;
  });
};

 addToCart=()=> {
        
        const { modalId } = this.state;
        this.setState((prev) => {
             
                  if (!prev.cart.includes(modalId )) {

            
                  return { cart: [...prev.cart,  modalId]}
               } 
             
            
         })
           this.setModalActive(false)
    }

    


  
 
 
  
  render() {

const modalActive = this.state.modalActive;
        const modalid = this.state.modalId;

    return (
        <>
             
        <section className="py-4 container">
          <div className="row juctify-content-center">
           <h1 className="text-center mt-3">
             <Header favCounter={this.state.favorits.length} cartCounter={this.state.cart.length} primary={this.state.favorits.length > 0}/>
            </h1>
            
            <ProductList
        
            cardProducts={this.state.cardProducts}
            favorits={this.state.favorits}
              addToFavorite={this.addToFavorite}
               setModalActive={this.setModalActive}
              
              
            />

              
            <Modal
            active={modalActive}
            id={modalid}
            headerText={this.state.headerText}
            mainText={this.state.mainText}
            addToCart={this.addToCart}
              onClick={() => this.setModalActive(false, 0)}
            />
       
      
          </div>
        </section>
      </>
    );
  }
}






