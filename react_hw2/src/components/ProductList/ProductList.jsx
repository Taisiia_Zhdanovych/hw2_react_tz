import { Component } from "react";

import CardItem from "../ProductItem/CardItem";




export default class productList extends Component {
    


   
    addToFavorite=(id)=> {
       
        this.props.addToFavorite(id);
    }


    setModalActive = (active, id) => { this.props.setModalActive(active, id) }
   

    
    render() {
       
            return (
               
                <>
                   

                    {this.props.cardProducts.map((item, id) => {
                        return (
                  
                          
                <CardItem
                
                      favorite={ this.props.favorits.includes(item.id)}
                      key={item.id}
                      {...item}
                  
                       addToFavorite={this.addToFavorite}
                      setModalActive={ this.setModalActive}
                    
                />
              );
                    })} 
                   
       
        
       
      
                </>
            )
        }
    }
