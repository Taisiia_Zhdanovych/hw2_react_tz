
import { Component } from "react";
import "./cardItem.scss";
import '@fortawesome/fontawesome-free/css/all.min.css';
import propTypes from "prop-types";




export default class CardItem extends Component {

  
    render() {
      
    
const starColor = this.props.favorite ? "star-active": "star-oncard"
    
    return (
      <div className="col-11 col-md-6 col-lg-4 mx-0 mb-5">
        <div className="card p-0 overflow-hidden h-100 shadow">
          <img src={this.props.url} alt="foto" className="card-img-top img-fluid"/>
                <div className="card-body">
            <i className={"fa-sharp fa-solid fa-star " + starColor}  key={this.props.id}  onClick={() => {this.props.addToFavorite(this.props.id)}} ></i>
                    <h5 className="card-title">{this.props.name}</h5>
                    <h6 className="card-title">артикул : {this.props.partNumber}</h6>
            <h5 className="card-title">$ {this.props.price}</h5>
                    <p className="card-text">{this.props.color}</p>
                  
                    <button className="btn btn-success" onClick={() => { this.props.setModalActive(true, this.props.id)}}>Add to Cart</button>
                    
          </div>
        </div>
      </div>
    );
  }
}

CardItem.defaultProps = {
  favorite: false,
};

CardItem.propTypes = {
    favorite: propTypes.bool,
    name: propTypes.string,
    color: propTypes.string,
    price: propTypes.number,
    partNumber: propTypes.string,
    id: propTypes.number,
    url: propTypes.string,
    setModalActive: propTypes.func,
    addToFavorite: propTypes.func
};