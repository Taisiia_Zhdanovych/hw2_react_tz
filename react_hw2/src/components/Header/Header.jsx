import { Component } from "react";
import './header.scss';
import propTypes from "prop-types";

export default class Header extends Component {
    constructor(props) {
    super(props)
    }
    render() {

        const starHeaderColor = this.props.primary  ? "header__star-active": "header__star"
        return (
            <>
                <div className="header">
                    
                        <p>Lovely Bags</p>
                    
                    
                    <ul className="navbar">
                        <li><i className={"fa-sharp fa-solid fa-star " + starHeaderColor}> </i>{this.props.favCounter}</li>
                       <li><i className="fa-solid fa-cart-shopping"> </i>{this.props.cartCounter}</li>
                       <li><h3>Контакти</h3></li>
                       <li><h3>Про нас</h3></li>
                    </ul>
                </div>  
            </>
        )
    }
}

Header.defaultProps = {
  primary: false,
  favCounter: 0,
  cartCounter: 0

};

Header.propTypes = {
  primary: propTypes.bool,
  favCounter: propTypes.number.isRequired,
  cartCounter: propTypes.number.isRequired,
};