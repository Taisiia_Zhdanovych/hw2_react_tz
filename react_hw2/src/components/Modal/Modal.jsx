import { Component } from "react";
import "./modal.scss";
import propTypes from "prop-types";



export default class Modal extends Component{
    
      
        
    
 
    render() {

        return (
            <div className={this.props.active ? "modal active" : "modal"} onClick={this.props.onClick}>
                <div className={this.props.active ? "modal__container active" : "modal__container"} onClick={e => e.stopPropagation()}>
                <div className="modal__header">
                <span className="modal__header-text">{this.props.headerText}</span>
                <span className={this.props.active ? "modal__close" : ""}  onClick={this.props.onClick}>X</span>
                </div>
                    <p className="modal__main-text">{this.props.mainText}
                </p>
                <div className="modal__btn-box">
                  <button className="modal__button" onClick={() => { this.props.addToCart(this.props.id)}}>Так</button>
                  <button className="modal__button" onClick={this.props.onClick}>Ні</button>
                </div>
              </div>
            </div>
        )
       
    }
  }

Modal.defaultProps = {
  active: false,
  headerText: "Modal Header",
  mainText: "Modal Main Text",

};

Modal.propTypes = {
     active: propTypes.bool.isRequired,
    headerText: propTypes.string,
    mainText: propTypes.string,
    id: propTypes.number,
    addToCart: propTypes.func,
   
};

